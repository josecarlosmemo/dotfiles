#!/bin/bash

# We set some user variables
alg_user='JMARTINE2'
alg_pass='manene205'


# We set some variables for the webpage
alg_login='https://c1-liceodelvalle.algebraix.com/bin/g/start/login_do/'
alg_grades='https://c1-liceodelvalle.algebraix.com/bin/s/gradebooks/period_details/?#/L2Jpbi9zL2dyYWRlYm9va3MvcGVyaW9kX2RldGFpbHMvP3hfbG9hZD02MjEuMDEwNjI5NTg5ODgm'

# Deleting the previous cookie file
rm -rf cookies.txt

# Getting the actual cookie file
echo "Logging into Algebraix..."
curl $alg_login --data "username=$alg_user&password=$alg_pass" -X POST -c cookies.txt -s > /dev/null

sleep 3

echo "Getting Grades..."

# Getting an actual text file with grades
curl $alg_grades -X GET -b cookies.txt -s | html2text -style compact |

# Formating & saving the text file
sed -n '/ ⁰ Mostrar sub-ciclos inactivos/,$p' |
tail -n+2 |
head -n-1 |
cut  -f 2 -d "*" > grades-backup.txt

# Comparing backup and actual grades
